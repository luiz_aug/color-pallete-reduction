import argparse
import colorsys
from pathlib import Path
from scipy.cluster.vq import kmeans2
from numpy.random import random_sample
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt
from matplotlib import colors as colors

def processInputFile(file_path):
    color_palette = []
    with open(file_path) as file:
        file_lines = file.read().splitlines()

    # Simple file verification
    for line in file_lines:
        if not (line.startswith("#")) or len(line) is not len("#rrggbb"):
            print("Input file error: " + line + " is not supported")
            return None
        else:
            color = line
            color_palette.append(color)

    return color_palette

def processColorPalette(original_color_palette, args):
    conversion_table = []
    color_data = [colors.hex2color(hex_color) for hex_color in original_color_palette]
    yiq_data = [colorsys.rgb_to_yiq(*rgb) for rgb in color_data]
    yiq_clusters, mapping = kmeans2(yiq_data, int(args.palette_size), minit="++")
    rgb_clusters = [colorsys.yiq_to_rgb(*yiq) for yiq in yiq_clusters]

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
    ax.scatter(*zip(*rgb_clusters), c=rgb_clusters, s=150, label="centroids")
    ax.scatter(*zip(*color_data), c=color_data, s=5, label="data")

    for o_data, idx_cluster in zip(color_data, mapping):
        cluster = rgb_clusters[idx_cluster]
        ax.plot(*zip(o_data, cluster), c=cluster)
        conversion_table.append(
            str(colors.rgb2hex(color_data[color_data.index(o_data)]))
            + " => "
            + str(colors.rgb2hex(rgb_clusters[idx_cluster]) + "\n")
        )

    if args.save_conversion_table:
        output_file_path = args.input_palette.split(".txt")[0] + "-converted" + ".txt"
        with open(output_file_path, "w") as file:
            for entry in conversion_table:
                file.write(entry)

    ax.set_xlabel("R")
    ax.set_ylabel("G")
    ax.set_zlabel("B")
    plt.legend()
    plt.show()

def main():
    desription_string = """ Reduce colors from a hex palette """
    input_palette_help = ("Input file that contains the palette to be reduced. Note: One color per line on hex format: #rrggbb")
    save_conversion_table_help = "Flag to store the conversion table. If set true, a file will be created on the same folder as the input collor palette file"
    palette_size_help = "Number of colors for the reduced palette"
    parser = argparse.ArgumentParser(description=desription_string)

    parser.add_argument(
        "--palette-size",
        dest="palette_size",
        required=True,
        help=palette_size_help,
    )
    parser.add_argument(
        "--input-palette",
        dest="input_palette",
        required=True,
        help=input_palette_help,
    )
    parser.add_argument(
        "--save-conversion-table",
        dest="save_conversion_table",
        required=False,
        action="store_true",
        help=save_conversion_table_help,
    )

    # COMMENT THIS TO USE AN EXAMPLE FILE
    args = parser.parse_args()

    # REMOVE BLOCK COMMENT TO USE AN EXAMPLE FILE 
    """  args = parser.parse_args(
        [
            "--palette-size",
            "4",
            "--input-palette",
            str(Path(__file__).absolute().parent) + "/inputPalette.txt",
            "--save-conversion-table",
        ]
    ) """

    input_palette = processInputFile(args.input_palette)
    if input_palette is not None:
        processColorPalette(input_palette, args)

if __name__ == "__main__":
    main()